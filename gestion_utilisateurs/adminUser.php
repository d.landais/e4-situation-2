<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Administration d'utilisateurs</title>
 		<?php include "../include/header_public.php";  ?>
		<link href="adminUser.css" rel="stylesheet">
	</head>

	<body>
		<h1>Gestion des utilisateurs</h1>

		<label>Ajoutr un utilisateur --></>
		<a href='Ajout.php'>Ajouter</a>
		<br>

		</form>
		
		<?php
		include '../include/connexionbdd.php';
		$requeteAffiche="select id_utilisateur, pseudo from jeux_video.utilisateur order by id_utilisateur";
		
		$resultats1= $connexion->query($requeteAffiche);
		
		$ligne=$resultats1->fetch();
		$id = 1;
		
		?>
	        <!-- Retour après éventuelle suppression pour affichage du message adéquate -->
		<?php
			// si la variable $_GET[ " message " ] existe (envoyée par suppression_profil.php  et n'est pas null
			if (isset($_GET["message"]))
			{
			   echo "<p style='color:red;'>".$_GET["message"]."</p>";
			} 
		?>
		<table style="text-align:left" border=1px bordercolor=blue>
		<?php
		while($ligne != null)
		{
			?>
			<tr>
				<td><!--id utilisateur-->
					<div class="id">
						<?php
						echo $ligne['id_utilisateur'];
						?>
					</div>
				</td>


				<td><!--pseudo-->
					<div class="pseudo">
						<?php
						echo $ligne['pseudo'];
						?>
					</div>
				</td>
				
				
				<td><!--bouton aperçu-->
					<form action='page_profil.php' method='post'>
						<div class="apercu">
							<input style="width: 126px;height: 35px;" type="submit" value="Aperçu" >
							<input type='hidden' name="user" value='<?php echo $ligne['id_utilisateur']; ?>' /> 
							<input type='hidden' name="admin" value='admin' /> 
						</div>
					</form>
				</td>
				
				
				<td><!--bouton modifier-->
					<form action='modification_profil.php' method='post'>
					   <div class="modifier">
					      <input style="width: 126px;height: 35px;" type="submit" value="Modifier" >
							<input type='hidden' name="id_modif" value='<?php echo $ligne['id_utilisateur']; ?>' /> 
					   </div>
					</form>
				</td>
				
				
				<td><!--bouton supprimer  :  lorsqu'on clic sur ce bouton, 
					on récupère l'id de l'utilisateur de la ligne et on appelle le fichier php qui supprimera cet utilisateur
					et reviendra sur cette page -->
					<form action='suppression_profil.php' method='post'>
						<div class="supprimer">
							<input style="width: 126px;height: 35px;" type="submit" value="Supprimer" >
							<input type='hidden' name="id_suppr" value='<?php echo $ligne['id_utilisateur']; ?>' /> 
						</div>
					</form>
				</td>
			</tr>
			<?php
			
			$ligne=$resultats1->fetch();
			$id += 1;
		}
		?>
		</table><br><br>
		<?php
		
		$resultats1->closeCursor();
		
		unset($connexion);
		?>
	<?php
		include '../include/footer_public.php';
	?>
	</body>
</html>

