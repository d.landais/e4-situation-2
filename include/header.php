<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Header CSS -->
   <link rel="stylesheet" href="./CSS/style_header.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./CSS/bootstrap.min.css">
    <!-- Lunar CSS -->
    <link rel="stylesheet" href="./CSS/lunar.css">
    <!-- Popup CSS - Modifié par Maxime & Nino -->
    <link rel="stylesheet" href="./CSS/style_popup.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="./CSS/animate.min.css">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Overpass:300,400,600,700,800,900" rel="stylesheet">
</head>
<body>
  <?php
  session_start();
  include 'connexionbdd.php';
  ?>
     <nav>
       <div class="Nav">
         <ul>
           <li>
             <div class="fond_image">
             <a href="./index.php"><img src="./images/main_logo.png" height="20"></a>
             </div>
           </li>
           <li><a href="./index.php">SIO Gaming</a></li>
           <li><a href="./gestion_utilisateurs/page_home_profil.php">Communauté</a></li>
           <li><a href="./statistique_jeu/statistique.php">Top Jeux</a></li>
           <?php
          if (isset($_SESSION['id_user'])){
           ?>
           <li><a href="./gestion_match/competition_show.php" id="compet">Compétitions</a></li>
           <?php } ?>
             <div class="fond_image">
             <a href="./gestion_jeu/recherche_test.php"><img src="./images/search_logo.png" height="20"></a>
             </div>
             <?php
            if (isset($_SESSION['id_user'])){
              if($_SESSION['role'] > 1){
             ?>
             <div class="fond_image">
             <a href="./panel/panel.php"><img src="./images/panel_logo.png" height="20"></a>
             </div>
           <?php }
         } ?>

           <?php
          if (isset($_SESSION['id_user'])){
           ?>
           <li><a href="./gestion_utilisateurs/usermodification.php"><?php echo $_SESSION['pseudo'] ?></a></li>
           <?php } ?>

             <li><a data-toggle="modal" id="lien_connexion" href="#connexion">
               <?php if (isset($_SESSION['id_user'])) { echo "Déconnexion"; } else { echo "Connexion"; } ?></a></li>
        </ul>
      </div>
    </nav>

<div id="tralala"> </div>
<?php if (!isset($_SESSION['id_user'])) {  ?>
<!-- POPUP CONNEXION -->
  <div class="modal fade" id="connexion" tabindex="-1" role="dialog" aria-labelledby="connexion" aria-hidden="true">
      <div class="modal-dialog  modal-dialog-centered" role="document">
          <div class="modal-content">
              <button type="button" class="close light" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>

              <div class="m-h-30 bg-img rounded-top" style="background-image: url('https://image.noelshack.com/fichiers/2020/06/4/1580979490-sio-gaming.png')">

              </div>
              <div class="modal-body">
                  <form class="px-sm-4 py-sm-4">
                      <center><h3>Connexion</h3></center>
                      <div class="form-group">

                          <label for="login">Login</label>
                          <input type="text" class="form-control" id="login" name="login" required="" placeholder="Saisir votre adresse mail / pseudonyme">
                      </div>

                      <div class="form-group">

                          <label for="password">Mot de passe</label>
                          <input type="password" class="form-control" id="password" name="password" required="" placeholder="Saisir votre mot de passe">
                          <small id="mdp_perdu" name="mdp_perdu" class="form-text text-muted"><a href="#">Mot de passe oublié</a></small>
                      </div>
                      <div class="form-row">
                      <button type="submit" class="btn btn-cstm-perso btn-cta testmax" id="btnconnexion" data-dismiss="modal" data-toggle="modal" >Connexion</button>
                      <button type="submit" class="btn btn-cstm-perso btn-cta testmax" data-dismiss="modal" data-toggle="modal" href="#inscription">Inscription</button>
        </div>
                  </form>
		<div id="message"> </div>
              </div>
          </div>
      </div>
  </div>
<!-- FIN POPUP CONNEXION -->

<!-- POPUP INSCRIPTION -->
  <div class="modal fade" id="inscription" tabindex="-1" role="dialog" aria-labelledby="inscription" aria-hidden="true">
      <div class="modal-dialog  modal-dialog-centered" role="document">
          <div class="modal-content">
              <button type="button" class="close light" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>

              <div class="m-h-30 bg-img rounded-top " style="background-image: url('https://image.noelshack.com/fichiers/2020/06/4/1580979490-sio-gaming.png')">

              </div>
              <div class="modal-body" style="overflow-y: scroll; height: 600px;">
                  <form class="px-sm-4 py-sm-4">
                      <center><h3>Inscription</h3></center>
                      <br>
                      <div class="form-group">

                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label for="nom">Nom</label>
                              <input type="text" class="form-control" id="nom" name="nom" required="" placeholder="Saisir votre nom">
                          </div>
                          <div class="form-group col-md-6">
                              <label for="prenom">Prénom</label>
                              <input type="text" class="form-control" id="prenom" name="prenom" required="" placeholder="Saisir votre prénom">
                          </div>
                      </div>


                      <div class="form-group">

                          <label for="email">Adresse mail</label>
                          <input type="email" class="form-control" id="email" name="email" required="" placeholder="Saisir votre adresse mail">
                      </div>

                      <div class="form-group">

                          <label for="pseudo">Nom d'utilisateur</label>
                          <input type="text" class="form-control" id="pseudo" name="pseudo" required="" placeholder="Saisir votre pseudonyme">
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label for="passwordi">Mot de passe</label>
                              <input type="password" class="form-control" id="passwordi" name="passwordi" required="" placeholder="Saisir le mot de passe">
                          </div>
                          <div class="form-group col-md-6">
                              <label for="passwordii">Confirmation</label>
                              <input type="password" class="form-control" id="passwordii" name="passwordii" required="" placeholder="Resaisir le mot de passe">

                          </div>
                      </div>

                      <div class="form-group">

                          <label for="datenaiss">Date de naissance</label>
                          <input type="date" class="form-control" id="datenaiss" name="datenaiss" required="" placeholder="Saisir la date de naissance">
                      </div>

                      <div class="form-group">
                      <label for="nationalite">Nationalité</label>

                      <select class="form-control" id="nationalite">
                        <?php
                        $requete = "SELECT id_nationalite, libelle FROM jeux_video.nationalite";
                        $resultats = $connexion->query($requete);
                        while($ligne = $resultats->fetch()){
                        echo "<option value=".$ligne['id_nationalite'].">".$ligne['libelle']."</option>";
                        }
                        ?>
                      </select>
                      </div>

                      <div class="form-group">

                          <label for="verif">Quelle est votre marque favorite de voiture ?</label>
                          <input type="text" class="form-control" id="verif" name="verif" required="" placeholder="Saisir votre réponse secrète">
                      </div>

                      <button type="submit" class="btn btn-cstm-perso btn-cta btn-block" data-dismiss="modal" id="btninscription">Inscription</button>
                  </form>
              </div>
                <div id="message2"> </div>
          </div>
      </div>
  </div>
</div>
<!-- FIN POPUP INSCRIPTION -->
<?php } ?>
<!-- POPUP JAVASCRIPT -->
<script src="./js/jquery.min.js"></script>
<script src="./js/popper.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/lunar.js"></script>
<script src="./js/jquery.js"></script>
